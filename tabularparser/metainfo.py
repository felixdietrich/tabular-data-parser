#
# Copyright The NOMAD Authors.
#
# This file is part of NOMAD. See https://nomad-lab.eu for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import typing

from nomad.datamodel.datamodel import EntryData
from nomad.metainfo import Quantity, Package, Reference, SectionProxy, MSection, SubSection


m_package = Package(
    name='example_nomadmetainfo_json',
    description='None')


class TableRow(EntryData):
    ''' Represents the data in one row of a table. '''
    table_ref = Quantity(
        type=Reference(SectionProxy('Table')),
        description='A reference to the table that this row is contained in.')


class Table(EntryData):
    ''' Represents a table with many rows and columns. '''
    row_refs = Quantity(
        type=Reference(TableRow.m_def), shape=['*'],
        description='References that connect to each row. Each row is stored in it individual entry.')


class TabularTreeNodeInfo(MSection):
    description = Quantity(type=str)
    unit = Quantity(type=str)


class TabularTreeLevel3(MSection):
    name = Quantity(type=str, default='<node name?>')
    info = SubSection(sub_section=TabularTreeNodeInfo)


class TabularTreeLevel2(MSection):
    name = Quantity(type=str, default='<node name?>')
    info = SubSection(sub_section=TabularTreeNodeInfo)
    nodes = SubSection(sub_section=TabularTreeLevel3, repeats=True)


class TabularTreeLevel1(MSection):
    name = Quantity(type=str, default='<node name?>')
    info = SubSection(sub_section=TabularTreeNodeInfo)
    nodes = SubSection(sub_section=TabularTreeLevel2, repeats=True)


class TabularTree(EntryData):
    name = Quantity(type=str, default='<node name?>')
    info = SubSection(sub_section=TabularTreeNodeInfo)
    nodes = SubSection(sub_section=TabularTreeLevel1, repeats=True)


class TabularMetaschema:
    def __init__(self, name, level, info):
        self.name = name
        self.info = info
        self.level = level
        self.nodes = []

    def tojson(self):
        result = {
            'name': self.name
        }
        if self.nodes:
            result['nodes'] = [
                node.tojson()
                for node in self.nodes
            ]
        if self.info:
            result['info'] = self.info
        return result

    def metainfo(self):
        return TabularTree.m_from_dict(self.tojson())

    def quantity(self):
        result_quantity = Quantity(name=self.name, type=str)
        if self.info and 'csv_column' in self.info:
            result_quantity.name = self.info['csv_column']
        if self.info and 'description' in self.info:
            result_quantity.description = self.info['description']
        if self.info and 'type' in self.info:
            result_quantity.type = self.info['type']
        if self.info and 'unit' in self.info:
            result_quantity.unit = self.info['unit']
        return result_quantity


m_package.__init_metainfo__()
