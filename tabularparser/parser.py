#
# Copyright The NOMAD Authors.
#
# This file is part of NOMAD. See https://nomad-lab.eu for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from typing import Union, List, Iterable, Dict
import csv
import os
import glob
import pandas as pd
import numpy as np

from nomad.datamodel import EntryArchive
from nomad.parsing import MatchingParser
from nomad.utils import get_logger
from nomad.units import ureg as units
from nomad.parsing.file_parser import UnstructuredTextFileParser, Quantity
from nomad.metainfo import Quantity, Package, Section

from .metainfo import Table, TableRow, TabularMetaschema
from . import metainfo  # pylint: disable=unused-import


def to_float(str_):
    try:
        if type(str_) is float or type(str_) is int:
            if np.isnan(str_):
                return None
            return str_
        float_val = float(str_.replace(",", "."))
        if np.isnan(float_val):
            float_val = 0.0  # TODO: this is necessary for JSON parsing, but dangerous in general!
        return float_val
    except ValueError:
        return None


def to_int(str_):
    try:
        if type(str_) is int or type(str_) is float:
            return str_
        return np.int(str_.replace(",", "."))
    except ValueError:
        return None


def to_str(float_):
    try:
        return str(float_)
    except ValueError:
        return None


def range_to_list(str_range):
    try:
        str_range = str_range.translate({ord(c): None for c in '()'})
        range = [to_float(f) for f in str_range.split("-")]
        if len(range) != 2:
            return None
        else:
            return range
    except AttributeError:
        if np.isnan(str_range):
            return None
        print(f"Error with range: {str_range}")
        return str_range


def remove_non_values(json_):
    keys = [key for key in json_.keys()]
    for key in keys:
        if json_[key] is None:
            json_.pop(key)
    return json_


def process_property(prop, row):
    """
    Converts a single property into a dictionary item.
    """
    index = prop["excel_row"] - 2
    if len(row) > index:
        value = row[index]
    else:
        value = None

    if prop["type"] == "string":
        value = to_str(value)
    elif prop["type"] == "float":
        value = to_float(value)
    elif prop["type"] == "int":
        value = to_int(value)
    elif prop["type"] == "range":
        value = range_to_list(value)

    prop_return = {
        "name": prop["property"],
        "value": value
    }
    if prop["description"] != "missing":
        prop_return["description"] = prop["description"]
    if prop["unit"] != "":
        prop_return["unit"] = prop["unit"]
    return prop_return


def extract_node_data(node):
    node_name = 'undefined'
    level = -1
    info = {}
    for idx_level in range(3):
        if type(node[idx_level]) is str:
            node_name = node[idx_level]
            level = idx_level
            break
    if type(node[3]) is str: info['description'] = node[3]
    if type(node[4]) is str: info['unit'] = node[4]
    if type(node[5]) is str: info['csv_column'] = node[5]
    if type(node[6]) is str: info['index'] = True

    return TabularMetaschema(
        name=node_name,
        level=level,
        info=info
    )


class TabularParser(MatchingParser):
    # TODO this is super simple and needs extension. Currently parses files like:
    #    header_0,header_1
    #    0_0,0_1
    #    1_0,1_1
    # TODO also extend tests/parsing/test_tabular
    def __init__(self) -> None:
        super().__init__(
            name='parser/tabular', code_name='tabular data',
            mainfile_name_re=r'.*\.csv$')

    def _read_metaschema_xlsx(self, filename):
        """
        Reads a given XLSX file metaschema.

        :param filename: path to XLSX file.
        """
        idx_row_definition = 2  # the row definition is written in this row of the metaschema file
        idx_column_definition = 4  # the column tree definitions start at this row

        df_notebook = pd.read_excel(filename, header=None)

        # read the row definition
        row_definition = df_notebook.iloc[idx_row_definition, 1]

        # read the column tree definitions
        metaschemas = []
        level_current = 1
        for idx_item in range(idx_column_definition, len(df_notebook)):
            node_data = df_notebook.iloc[idx_item, :]
            node = extract_node_data(node_data)
            if level_current <= node.level:
                metaschemas[-1].nodes.append(node)
            if level_current > node.level:
                metaschemas.append(node)
            level_current = node.level

        tree = TabularMetaschema(name='root',
                                 level=-1,
                                 info={
                                     'description': row_definition
                                 })
        tree.nodes = metaschemas
        return tree

    def _normalize_data(self, metaschema, data, data_columns=[]):
        if 'csv_column' in metaschema.info and metaschema.info['csv_column'] in data.keys():
            metaschema.info['type'] = str
            if 'int' in str(data[metaschema.info['csv_column']].dtypes):
                metaschema.info['type'] = str  # TODO: this should be int
            if 'float' in str(data[metaschema.info['csv_column']].dtypes):
                metaschema.info['type'] = str  # TODO: this should be float, but NaN cannot be handled
            data_columns.append(metaschema)
        for node in metaschema.nodes:
            self._normalize_data(node, data, data_columns)
        return metaschema

    def _read_data_file(self, filename):
        """
        Read the csv or xls/xlsx with all measurement data.

        :param filename: measurement file with headers in first row.
        :return: list of json objects, one for each material.
        """
        if filename[-3:].lower() == 'csv':
            data_parsed = pd.read_csv(filename)
        elif filename[-3:].lower() == 'xls' or filename[-4:].lower() == 'xlsx':
            df_notebook = pd.read_excel(filename, header=0)
            data_parsed = df_notebook
        else:
            raise ValueError(f'{filename} is not of csv or xls/xlsx type.')
        return data_parsed

    def is_mainfile(
        self, filename: str, mime: str, buffer: bytes, decoded_buffer: str,
        compression: str = None
    ) -> Union[bool, Iterable[str]]:
        # We use the main file regex capabilities of the superclass to check if this is a
        # .csv file
        is_tabular = super().is_mainfile(filename, mime, buffer, decoded_buffer, compression)
        if not is_tabular:
            return False

        try:
            rows = self._read_data_file(filename)
        except Exception:
            # If this cannot be parsed as a .csv file, we don't match with this file
            return False

        return [str(item) for item in range(rows.shape[0])]

    def parse(
        self, mainfile: str, archive: EntryArchive, logger=None,
        child_archives: Dict[str, EntryArchive] = None
    ):
        # check if there is an additional metainfo file.
        metainfo_path = next(iter(
            glob.glob(os.path.join(os.path.dirname(mainfile), '*_metainfo.xlsx'))), None)
        if metainfo_path:
            metainfo = self._read_metaschema_xlsx(metainfo_path)
        else:
            logger.warning('No *_metainfo.xlsx file found, aborting')
            return

        data = self._read_data_file(mainfile)
        data_columns = []
        data_with_info = self._normalize_data(metainfo, data, data_columns)

        table_row_def = Section(name='TableRow', base_sections=[TableRow.m_def])
        table_row_def.quantities = [column.quantity() for column in data_columns]

        archive.definitions = Package(section_definitions=[table_row_def])
        MyTableRow = table_row_def.section_cls

        # Create a Table as the main archive contents. This can hold references to the
        # rows.
        table = Table()
        archive.data = table
        table_rows = []

        headers = [col_name for col_name in data.keys()]

        # Create the data for each row by instantiating the generated schema and store it
        # in the child archives. Use references to connect with the Table section in the
        # main archive.
        for index, row in data.iterrows():
            key = str(index)
            archive = child_archives[key]
            try:
                row_str = [str(r) for r in row]
                archive.data = MyTableRow(table_ref=table, **dict(zip(headers, row_str)))  # type: ignore
                table_rows.append(archive.data)
            except TypeError:
                print(f'Type error for row {row}')
        table.row_refs = table_rows
